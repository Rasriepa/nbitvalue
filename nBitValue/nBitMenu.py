from nBitValue import nBitValue as nBit
import nBitValueFunctions as nf

def main():
    # Variables
    # functions array: 0 = display name, 1 = description, 2 = required parameters
    f0 = ["display value\t\t","|takes a decimal number and prints its hexadecimal and binary representation.","|dec"]
    f1 = ["Square 'n Multiply\t","|effectively powers number a by number b in F_p\t\t\t\t\t\t\t\t","|a,b,p"]
    f2 = ["EEA\t\t\t\t","|extended Euklidean algorithm, shows gdc and bezout coefficients\t\t\t","|a,b"]
    f3 = ["RSA-Encrypt\t\t","|encrypts a single decimal or a string with RSA algorithm\t\t\t\t\t","|x, k_priv, p"]
    f4 = ["Baby-Steps Giant-Steps","|efficiently calculates discrete logarithm a ** x = g mod p\t\t\t\t\t","|p,g,a"]
    f5 = ["Find elements\t\t","|finds all elements that a creates in F_p\t\t\t\t\t\t\t\t\t","|a,p"]
    f6 = ["Find order\t\t\t","|finds order of a in F_p, i.e. how many elements a sub-group has.\t\t\t","|a,p"]
    functions = [f0,f1,f2,f3,f4,f5,f6]

    def welcome(functions):
        print("= " * 16, "\n=*=*=*=*= RSA-Riepa =*=*=*=*=*=\n=","= " * 15)
        print(f'Python-based crypto-tool -alpha-\nAvailable functions:\t\t{len(functions)}\n')

    def menu(functions):
        print(f'#\tfunction name:\t\t\tfunction description:\t\t\t\t\t\t\t\t\t\t\t\t\t\t\trequired params:\n',"-"*128)
        for f in functions:
            print(f'-{functions.index(f)}-| {f[0]}\t{f[1]}\t{f[2]}')
        choice = int(input(f"\nWhich function to use? (0) - ({functions.index(functions[-1])})"))
        if  choice >= len(functions) or  choice < 0:
            print("Sorry, this function is not available. Pick another one!\n")

        if choice == 0:
            displayValue()
        if choice == 1:
            SAM()
        if choice == 2:
            eea()
        if choice == 3:
            RSA_enc()
        if choice == 4:
            bs_gs()
        if choice == 5:
            find_elements()
        if choice == 6:
            find_order()

    def displayValue():
        n = nBit(dec=int(input("Please enter your decimal value:")))
        nf.displayValues([n])

    def SAM():
        a = nBit(dec=int(input("Enter a:")))
        b = nBit(dec=int(input("Enter b:")))
        p = nBit(dec=int(input("Enter p:")))
        output = nf.sqm(a,b,p)
        nf.displayValues([output])

    def eea():
        a = nBit(dec=int(input("Enter a:")))
        b = nBit(dec=int(input("Enter b:")))
        output = nf.eea(a,b)
        print(f'\nLine 0: gdc ({a.getDec()},{b.getDec()}), Line 1: multiplicative inverse of {a.getDec()} in F_{b.getDec()}')
        nf.displayValues(output)

    def RSA_enc():
        choice = int(input("Encrypt decimal (0) or string (1)?"))
        if not (choice == 0 or choice == 1):
            print("Invalid input, quitting to main.")
            return
        if choice == 0:
            x = nBit(dec=int(input("Enter x:")))
            k_priv = nBit(dec=int(input("Enter k_priv:")))
            p = nBit(dec=int(input("Enter p:")))
            output = nf.sqm(x,k_priv,p)
            print(f' {x.getDec()} en-/decrypted with {k_priv.getDec()}:')
            nf.displayValues([output])
            return output
        if choice == 1:
            userinput = input("Enter message (a-z,A-Z):")
            x = nf.convertStringDec(userinput)
            k_priv = nBit(dec=int(input("Enter k_priv:")))
            p = nBit(dec=int(input("Enter p:")))
            output = []
            for ele in x:
                output.append(nf.sqm(ele,k_priv,p))
            print(f'{userinput} en-/decrypted with d={k_priv.getDec()} mod {p.getDec()}:')
            nf.displayValues(output)
            return output

    def bs_gs():
        p = nBit(dec=int(input("Enter p:")))
        g = nBit(dec=int(input("Enter g:")))
        a = nBit(dec=int(input("Enter a:")))
        print(f'Solving discrete logarithm {a.getDec()} ** x = {g.getDec()}')
        output = nf.babysteps_giantsteps(g,a,p)
        nf.displayValues([output])
        return output

    def find_elements():
        a = nBit(dec=int(input("Enter a:")))
        p = nBit(dec=int(input("Enter p:")))
        op = int(input("Additive (0) or multiplicative (1)?"))
        if not (op == 0 or op == 1):
            print("Invalid input, quitting to main.")
            return
        if op == 0:
            operation = "+"
        if op == 1:
            operation = "*"
        output = nf.find_elements(p,a,operation)
        nf.displayValues(output)
        return output

    def find_order():
        a = nBit(dec=int(input("Enter a:")))
        p = nBit(dec=int(input("Enter p:")))
        op = int(input("Additive (0) or multiplicative (1)?"))
        if not (op == 0 or op == 1):
            print("Invalid input, quitting to main.")
            return
        if op == 0:
            operation = "+"
        if op == 1:
            operation = "*"
        output = nf.find_ord(p,a,operation)
        nf.displayValues([output])
        return output

    welcome(functions)          # actual main-loop
    while True:
        menu(functions)


main()