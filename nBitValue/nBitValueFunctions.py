from nBitValue import nBitValue as nBit
verbose = True

def createArray(n: int) -> list:
    # if verbose:
        # print("DEBUG_createArray: creating",n,"empty nBit values.")
    output = []
    for i in range(0,n):
        output.append(nBit(bin=[0,0,0,0,0,0,0,0]))
    return output

def createRandomValues(n: int) -> list:
    from random import randint
    if verbose:
        print("DEBUG_createRandomValues: generating", n, "random nBit Values.")
    output = []
    for i in range(0,n):
        r = int(randint(0,255))
        name = nBit(dec=r)
        output.append(name)
    return output

def displayValues(array: list):
    s = ""
    for i in range(len(array)):
        s += (" Dec: " + str(array[i].getDec()) + " \t Hex: " + str(array[i].getHex()) + " \t Bin: " + str(array[i].getBin()) + "\n")
    print(s)

def xorBin(nValue1: nBit, nValue2: nBit) -> list:
    bin1 = nValue1.getBin()
    bin2 = nValue2.getBin()
    assert len(bin1) == len(bin2), "Both binaries must be the same length"
    if verbose:
        print("DEBUG_xorBIN: bin1: ", bin1)
        print("DEBUG_xorBIN: bin2: ", bin2)
    r = []
    for i in range(len(bin1)):
        r.append(int(bin1[i] ^ int(bin2[i])))
    return r

def xorHex(nValue1: nBit, nValue2: nBit) -> list:
    return xorBin(nValue1, nValue2)

def xorArrayKey(array: list, key: nBit):
    output = createArray(len(array))
    for i in range(len(output)):
        output[i].setBin( xorBin(array[i], key))
        output[i].update()
    return output

def xorArrays(array1: list, array2: list) -> list:
    assert len(array1) == len(array2), "Arrays must be of same size"
    output = createArray(len(array1))
    for i in range(len(output)):
        output[i].setBin(xorBin(array1[i], array2[i]))
        output[i].update()
    return output

def convertStringDec(string: str) -> list:
    assert type(string) == str, "Must be a string."
    output = createArray(len(string))
    map = {"a": 0, "b": 1, "c": 2, "d": 3, "e": 4, "f": 5, "g": 6, "h": 7, "i": 8, "j": 9, "k": 10, "l": 11, "m": 12,
           "n": 13, "o": 14, "p": 15, "q": 16, "r": 17, "s": 18, "t": 19, "u": 20, "v": 21, "w": 22, "x": 23, "y": 24,
           "z": 25}
    map2 = {"A": 0, "B": 1, "C": 2, "D": 3, "E": 4, "F": 5, "G": 6, "H": 7, "I": 8, "J": 9, "K": 10, "L": 11, "M": 12,
            "N": 13, "O": 14, "P": 15, "Q": 16, "R": 17, "S": 18, "T": 19, "U": 20, "V": 21, "W": 22, "X": 23, "Y": 24,
            "Z": 25}
    s = 0
    for i in range(len(string)):
        if int(ord(string[i])) >= 97 and int(ord(string[i])) <= 122:
            s = map[str(string[i])]
        if int(ord(string[i])) >= 65 and int(ord(string[i])) <= 90:
            s = map2[str(string[i])]
        if int(ord(string[i])) == 32:
            raise ("Do not use space in string for converstions.")

        if (int(ord(string[i])) < 65 or int(ord(string[i])) > 90) and (int(ord(string[i])) < 97 or int(ord(string[i])) > 122) and (int(ord(string[i])) != 32):
            raise ("Can only convert a-z and A-Z")
        output[i].setDec(s)
        output[i].update()
    return output

def convertDecString(array: list) -> str:
    assert type(array) == list, "Input must be list of nBitValues"
    map = {"a": 0, "b": 1, "c": 2, "d": 3, "e": 4, "f": 5, "g": 6, "h": 7, "i": 8, "j": 9, "k": 10, "l": 11, "m": 12,
           "n": 13, "o": 14, "p": 15, "q": 16, "r": 17, "s": 18, "t": 19, "u": 20, "v": 21, "w": 22, "x": 23, "y": 24,
           "z": 25}
    s = ""
    for i in range(len(array)):
        assert type(array[i]) == nBit, "Input must be array of nBitValues, input contains at least 1 non-nBitValue"
        assert (array[i].getDec() >= 0) and (array[i].getDec() <= 26), "Input must be between 0 - 26."
        keys = list(map.keys())
        s += keys[array[i].getDec()]
    return s

def cesarEnc(array: list, key:nBit) -> list:
    output = createArray(len(array))
    k = key.getDec()
    for i in range(len(array)):
        x = array[i].getDec()
        y = ((x + k) % 27)
        output[i].setDec(y)
        output[i].update()
    return output

def cesarDec(array: list, key:nBit) -> list:
    output = createArray(len(array))
    k = key.getDec()
    for i in range(len(array)):
        y = array[i].getDec()
        x = ((y-k % 27) % 27)
        output[i].setDec(x)
        output[i].update()
    return output

def createKeyPairs(n:int) -> list:
    r = []
    n_temp = nBit(dec=n)
    for i in range(n):
        i_temp = nBit(dec=i)
        kp = eea(n_temp,i_temp)
        if kp[0].getDec() == 1:
            r.append([i,kp[2]])
    output = []
    for i in range(len(r)):
        e = nBit(dec=int(r[i][0]))
        d = nBit(dec=int(r[i][1].getDec()))
        output.append([e,d])
    return output

def find_ord(_n: nBit, _a: nBit, operation: str):
    n = _n.getDec()
    a = _a.getDec()
    output = []
    for i in range(1,n):
       if operation == "+":
           if (a * i) % n == 0:
                output.append(i)
       if operation == "*":
           if (a ** i) % n == 1:
                output.append(i)
    return nBit(dec=min(output))

def find_elements(_n: nBit, _a: nBit, operation: str):
    n = _n.getDec()
    a = _a.getDec()
    output = []
    for i in range(1,n):
        if operation == "+":
            if not (a*i) % n in output:
                output.append((a*i) % n)
        if operation == "*":
            if not (a**i) % n in output:
                output.append((a**i) % n)
    output.sort()
    real_output = []
    for elem in output:
        real_output.append(nBit(dec=elem))
    return real_output

def eea(nBit1: nBit, nBit2: nBit) -> list:
    dec1 = nBit1.getDec()
    dec2 = nBit2.getDec()
    r = [dec1, dec2]
    s = [1,0]
    t = [0,1]
    i = 1
    if verbose:
        print(f'EEA ({dec1},{dec2}):')
    while r[i] != 0:
        i +=1
        r.append(int(r[i-2] % r[i-1]))
        q = int((r[i-2]-r[i]) / r[i-1])
        s.append(int(s[i-2]-(q*s[i-1])))
        t.append(int(t[i-2]-(q*t[i-1])))
        if verbose:
            print(f'i: {i}\t r:{r[i]}\t q:{q}\t s:{s[i]}\t t:{t[i]}')
    output = createArray(3)
    output[0] = nBit(dec=r[i-1])
    output[1] = nBit(dec=(s[i-1])% dec2) # Inverse zu nBit1 in p_nBit2
    output[2] = nBit(dec=t[i-1])
    return output

def sqm(_x: nBit, _h: nBit, _mod: nBit) -> nBit:
    """
    powers 2 numbers effectively
    """
    x = _x.getDec()
    h = _h.getBin()
    mod = _mod.getDec()
    r = x
    if verbose:
        print("Start SQM x: \t",x," ^",_h.getDec(),"\t \t mod",mod)
        print(_h.getDec()," in binary:",h)
    for i in range(h.index(1)+1,len(h)):
        if verbose:
            print("SQ: \t", r**2 % mod, " \t= ", r, "^2 = ",r**2,"\t mod ",mod )
        r = r**2 % mod

        if h[i] == 1:
            if verbose:
                print("M: \t \t",r * x % mod," \t= ", r ,"*", x,"\t \t \tmod ",mod)
            r = r * x % mod
    print("Result: ",r,"mod",mod)
    return nBit(dec=r)

def elgamal_enc( alpha: nBit, k_pr: nBit, i: nBit, x: nBit, p: nBit,) -> nBit:
    if verbose:
        print("Berechne K_pub:")
    k_pub = sqm(alpha, k_pr, p)
    if verbose:
        print("Berechne K_E: ")
    k_E = sqm(alpha, i, p)
    if verbose:
        print("Berechne k_M: ")
    k_M = sqm(k_pub, i, p)
    if verbose:
        print("Verschlüssele x mit K_M:")
    y = nBit(dec=(x.getDec() * k_M.getDec() % p.getDec()))
    if verbose:
        print(y.getDec(), "= ", x.getDec(), " * ", k_M.getDec(), "mod", p.getDec())
    return y

def elgamal_dec(alpha: nBit, k_pr: nBit, i: nBit, y: nBit, p: nBit) -> nBit:
    if verbose:
        print("Entschlüsselung: \n Bestimme Inverse von K_M: ")
    k_pub = sqm(alpha, k_pr, p)
    k_E = sqm(alpha, i, p)
    k_M = sqm(k_pub, i, p)
    k_M_inv = eea(p, k_M)[2]
    if verbose:
        print("EEA(p, k_M): ", k_M_inv.getDec())
        print("entschlüssele: ",y.getDec()," * ",k_M_inv.getDec()," mod ",p.getDec(),": ")
    x_ = y.getDec() * k_M_inv.getDec() % p.getDec()
    if verbose:
        print("X' :", x_)
    return nBit(dec=x_)

def babysteps_giantsteps(_g: nBit, _a: nBit, _p: nBit) -> nBit:
    from math import ceil
    from math import sqrt
    p = _p.getDec()
    g = _g.getDec()
    a = _a.getDec()
    n = p - 1
    m = ceil(sqrt(n))
    L1 = [0] * m
    L2 = [0] * m
    if verbose:
        print("a:",a,"\ng:", g,"\np:",p,"\tord(",p,"):",n)

        print("(g ** j) % p: ")
    for j in range(m):
        L1[j] = (g ** j) % p
    if verbose:
        print("L1:",L1)
        print("a * (g ** -m) ** i % p:")
    for i in range(m):
        temp = a*(pow(g,-m,p)**i) % p
        L2[i] = temp
        if temp in L1:
            output = i*m+L1.index(temp) % p
            if verbose:
                print("L2:", L2)
                print("Found a match:",temp,"\n",output,"=",i,"*",m,"+",L1.index(temp),"mod",p)
    print(g,"** ",output,"mod ",p," = ",a,"\nx = ",output)
    return nBit(dec=output)

def elgamal_sig(_d: nBit, _alpha: nBit, _x: nBit, _k_E, _p: nBit):
    assert _d.getDec() <= _p.getDec() - 2, "Invalid d value!, must be max. p-2."
    x = _x.getDec()
    alpha = _alpha.getDec()
    d = _d.getDec()
    p = _p.getDec()
    if verbose:
        print(f'Beta = alpha ** d mod p:')
    beta = sqm(_alpha, _d, _p)
    if verbose:
        print(f'Public Key: ({_p.getDec()}, {_alpha.getDec()}, {beta.getDec()})')
    k_E = _k_E.getDec()
    eea_output = eea(_k_E, nBit(dec=p-1))
    if not eea_output[0].getDec() == 1:
        print(f"Invalid k_E, gdc(k_E,p-1) must be 1 but is {eea_output[0].getDec()}!")
        return
    else:
        print("K_e is valid!")
    k_E_inv = eea_output[1].getDec() % p-1
    if verbose:
        print(f'r = alpha ** k_E mod p:')
    r = sqm(_alpha, _k_E, _p)
    s = nBit(dec=((x-(d*r.getDec())) * k_E_inv) % (p-1))
    if verbose:
        print(f's = ({x}-({d}*{r.getDec()})) * {k_E_inv} mod {p-1}:\n  = {s.getDec()}')
        print(f'Signature: ({x},{r.getDec()},{s.getDec()})')
    output = createArray(3)
    output[0] = _x
    output[1] = r
    output[2] = s
    return output

def elgamal_ver(_alpha: nBit, _beta: nBit, _m: list, _p: nBit):
    x = _m[0]
    r = _m[1]
    s = _m[2]
    print(f'Verifying (beta ** r) * (r**s) = alpha ** x:')
    t = sqm(_beta, r, _p).getDec() * sqm(r, s, _p).getDec() % _p.getDec()
    ver = sqm(_alpha, x, _p).getDec()
    if t == ver:
        print(f'{t} = {ver}\nSignature valid!')
        return True
    else:
        print(f'{t} != {ver}\nSignature invalid!')
        return False

def elgamal_fakesig(_alpha: nBit, _beta: nBit, _i: nBit, _j: nBit, _p: nBit):
    p = _p.getDec()
    alpha = _alpha.getDec()
    beta = _beta.getDec()
    i = _i.getDec()
    j = _j.getDec()
    print(f'Faking signature with Elgamal for \nk_pub = (p, alpha, beta) = ({p},{alpha},{beta}):\n')
    r_1 = sqm(_alpha, _i, _p).getDec()
    r_2 = sqm(_beta, _j, _p).getDec()
    r = ( r_1 * r_2 ) % (p)
    print(f'r = ({alpha} ** {i}) * ({beta} ** {j}) mod {p}\n  = {r_1} * {r_2} = {r} mod {p}')
    j_inv = eea(_j, nBit(dec=_p.getDec() - 1))[1].getDec()
    s = (-(r) * j_inv) % (p-1)
    print(f's = -{r} * {j_inv} \tmod {p-1}: \n  = {s} \t\t\tmod {p-1}')
    x = (s * i) % (p-1)
    print(f'x = {s} * {i} \tmod {p-1}\n  = {x} \t\t\tmod {p-1}\nFake-Signature: ({x},{r},{s})\n')
    output = [nBit(dec=x),nBit(dec=r),nBit(dec=s)]
    if elgamal_ver(_p, _alpha, _beta, output):
        return output

def collision_probability(_lambda, _n):
    from math import log2
    from math import log
    from math import sqrt
    t = 2 ** ((_n+1)/2) * sqrt(log(1/(1-_lambda)))
    exponent = log2(t)
    print(f't = {t} which is 2 ** {exponent}.')

