from Mathe2 import is_prime as is_prime                 # importing tool-function(s)
from nBitValue import nBitValue
from nBitValueFunctions import eea as eea
verbose = True
class ECC_dot:                                          #   Class for handling dots on a elliptic curve
    def __init__(self, _x=0, _y=0, _isO=False):
        self._m_x = int(_x)                             #   Variables
        self._m_y = int(_y)
        self._m_isO = bool(_isO)

    def getX(self):                                     # getter-methods
            return self._m_x
    def getY(self):
        return self._m_y
    def getIsO(self):
        return self._m_isO
    def display(self):                                  # display method
        if self._m_isO:
            print("Big O")
        else:
            print(self._m_x,self._m_y)


class ell_Curve:                                        # Class for representing elliptic curves
    def __init__(self, _a: int, _b: int, _p: int):
        self._m_a = int(_a) % _p                        # Variables mod p, to handle negative values easily
        self._m_b = int(_b) % _p
        self._m_p = int(_p)
        self.validateCurve()
        self._m_validDots = self.createValidDots()

    def validateCurve(self):
        if ( 4*(self._m_a**3) + 27*(self._m_b**2) ) % self._m_p == 0:
            raise "Not a valid curve, modify a or b."   # making sure the criteria for a valid elliptic curve are met
        if not is_prime(self._m_p):
            raise "Not a valid curve, p is not prime."

    def getA(self):                                     # getter methods
        return self._m_a
    def getB(self):
        return self._m_b
    def getP(self):
        return self._m_p
    def createValidDots(self):                          # creates a list with all valid dots in the curve
        output = [ECC_dot(0,0,True)]
        root_table = []
        for i in range(0,self._m_p):
            root_table.append((i,i**2 % self._m_p))
        for j in range(0,self._m_p):
            for k in range(len(root_table)):
                if (j**3+self._m_a * j + self._m_b) % self._m_p == root_table[k][1]:
                    output.append( ECC_dot(j,root_table[k][0]))
        return output
    def getValidDots(self):                             # returns valid dots for sanity check later
        return self._m_validDots
    def getOrder(self):                                 # returns amount of valid dots in curve, aka order
        return len(self._m_validDots)
    def getHasseRange(self):
        from math import sqrt as sqrt
        from math import floor as floor
        lower = floor(self._m_p + 1 - 2* sqrt(self._m_p))
        upper = floor(self._m_p + 1 + 2* sqrt(self._m_p))
        print(f' possible orders for elements in E are from {lower} to {upper}')
    def display(self):
            print(f'elliptic curve: x^3+{self._m_a}*x^2+{self._m_b} mod {self._m_p}')

def addDots(P: ECC_dot, Q: ECC_dot, E: ell_Curve) -> ECC_dot:   #function for adding dots
    x1 = P.getX()                                               # variables
    x2 = Q.getX()
    y1 = P.getY()
    y2 = Q.getY()
    a = E.getA()
    b = E.getB()
    p = E.getP()
    validDots = E.getValidDots()
    if verbose:
        print(f'Adding ({P.getX()},{P.getY()}) + ({Q.getX()},{Q.getY()}) in ')
        E.display()
    if P.getIsO():                                      # handling "O", neutral element in the curve by
        return Q                                        # returning P if Q = 0 and Q if P = 0
    if Q.getIsO():
        return P
    if y1 == -y2 % p and x1 == x2:
        return ECC_dot(_isO=True)
    if x1 == x2 and y1 == y2 and P.getIsO() == False and Q.getIsO() == False:           # dot multiplication
        inverse = eea(nBitValue(dec=(2*y1) % p),nBitValue(dec=p))[1]
        s =  ((3*(x1**2) +a) % p * (inverse.getDec()) % p ) % p
        if verbose:
            # print(f's: {s} \tinv.: {inverse.getDec() % p} \tx1: {x1} \tx2: {x2} \ty1: {y1} \ty2: {y2} 3*(x1**2)+a% p: {(3*(x1**2) + a) % p }')
            print(f's =  (3*(x^2)+a) \n    ------------- mod {p} \n \t\t(2*y1) \n  = {(3*(x1**2) +a) % p } * {inverse.getDec() %p } mod {p} \n  = {s} ')
    else:
        inverse = eea(nBitValue(dec=(x2 - x1) % p), nBitValue(dec=p))[1]
        s = (((y2 - y1) % p) * inverse.getDec()) % p
        if verbose:
            # print(f's: {s} \tinv.: {inverse.getDec() % p} \tx1: {x1} \tx2: {x2} \ty1: {y1} \ty2: {y2} ((y2 - y1) % p: {((y2 - y1) % p) % p }')
            print(f's = (y2-y1) \n    ------- mod {p} \n \t(x2-x1) \n  = {(y2-y1) % p} * {inverse.getDec() % p} mod {p} \n  = {s} ')
    x3 = (s**2 - x1 - x2) % p
    y3 = (s*(x1 - x3) - y1) %p
    if verbose:
        print(f'x3 = (s^2 - x1- x2) \t\tmod p = {x3} \ny3 = (s*(x1 - x3) - y1) \tmod p = {y3}')
    for i in range(len(validDots)):                     # Sanity check
        if validDots[i].getX() == x3 and validDots[i].getY() == y3:
            return ECC_dot(x3,y3)
    raise "Something went wrong, result is not a valid dot in E."

def multiplyDots(P: ECC_dot, E: ell_Curve, n: int):     # multiplies the same dot n times
    if verbose:
        print(f'{n} * ({P.getX()},{P.getY()}):')
    P_temp = P
    for i in range(n-1):
        P_temp = addDots(P_temp,P,E)
    print(f'Result of {n} * ({P.getX()},{P.getY()}):')
    P_temp.display()
    return P_temp


def doubleAdd(P: ECC_dot, E: ell_Curve, n: int):
    if verbose:
        print(f'{n} * ({P.getX()},{P.getY()}):')
    bin = nBitValue(dec=n).getBin()
    P_temp = P
    for i in range(bin.index(1) + 1, len(bin)):
        P_temp = addDots(P_temp, P_temp, E)
        if bin[i] == 1:
            P_temp = addDots(P_temp, P, E)
    return P_temp

def orderPinE(P: ECC_dot, E: ell_Curve):                # bonus: finds order of dot P in curve E: how many times is P multiplied, until it becomes "Big O"?
    P_temp = P
    for i in range(E.getOrder()):
        P_temp = addDots(P_temp,P,E)
        if P_temp._m_isO == True:
            return i+1


def ECDLP_bf(P: ECC_dot, T: ECC_dot, E: ell_Curve):      # finds d, private key, how many times has P been multiplied to be T, by brute force
    P_temp = P
    for i in range(E.getOrder()):
        P_temp = addDots(P_temp,P,E)
        if P_temp.getX() == T.getX() and P_temp.getY() == T.getY():
            return i+1

