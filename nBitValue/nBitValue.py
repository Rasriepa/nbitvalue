verbose = False

class nBitValue:
    """
    Class to handle decimal, hexadecimal and binary values.
    """
    def __init__(self, dec=0, bin=[], hex=""):
        self.m_dec = dec
        self.m_bin = bin
        self.m_hex = hex
        self.m_new = True
        self.m_n = 0
        self.m_last_changed = ""
        if not dec == 0:
            self.m_last_changed = "dec"
        if not bin == []:
            self.m_last_changed = "bin"
        if not hex == "":
            self.m_last_changed = "hex"
        self.update()

    def getDec(self):
        return self.m_dec

    def getBin(self):
        return self.m_bin

    def getHex(self):
        return self.m_hex

    def last_changed(self):
        return self.m_last_changed

    def getN(self):
        return self.m_n

    def setDec(self, dec: int):
        self.m_dec = dec
        self.changed("dec")

    def setBin(self, bin: list):
        self.m_bin = bin
        self.changed("bin")

    def setHex(self, hex: str):
        self.m_hex = hex
        self.changed("hex")

    def setN(self):
        bin = self.getBin()
        self.m_n = len(bin)

    def changed(self, type: str):
        self.m_last_changed = type
        self.new = False

    def convertDecBin(self):
        bin = []
        n = self.getDec()
        if n == 0:
            bin = [0]
        d = 0
        r = 0
        while n > 0:
            d = n // 2
            r = (n % 2 )
            bin.insert(0,r)
            n = d
        binsize = len(bin)
        if binsize % 8 != 0:
            for i in range(binsize%8,8):
                bin.insert(0,0)
        self.setBin(bin)
        self.changed("bin")
        if verbose:
            print("DEBUG_convertDecBin: Converted", self.getDec(), "to ", bin)

    def convertBinDec(self):
        dec = 0
        bin = self.getBin()
        binsize = len(bin)
        for i in range(0,binsize):
            if bin[binsize-i-1] == 1:
                dec += 2**i
        self.setDec(dec)
        self.changed("dec")
        if verbose:
            print("DEBUG_convertBinDec: Converted ", bin, " to ", dec)

    def convertBinHex(self):
        map = {"0000":"0","0001":"1","0010":"2","0011":"3","0100":"4","0101":"5","0110":"6","0111":"7","1000":"8","1001":"9","1010":"A","1011":"B","1100":"C","1101":"D","1110":"E","1111":"F"}
        bin = self.getBin()
        hex = ""
        dec = ""
        assert(len(bin)) % 4 == 0, "Binary value must be of 4 digits or multiply of 4"
        for i in range(0,len(bin)):
            if i % 4 == 0:
                bin0 = str(bin[i])
                bin1 = str(bin[i+1])
                bin2 = str(bin[i+2])
                bin3 = str(bin[i+3])
                dec = bin0 + bin1 + bin2 + bin3
                hex += map[dec]
        self.setHex(hex)
        self.changed("hex)")
        if verbose:
            print("DEBUG_convertBinHex: Converted", self.getBin(), "to ", hex)

    def convertHexDec(self):
        dec = 0
        map = {"0": 0, "1": 1, "2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9, "A": 10, "B": 11, "C": 12, "D": 13, "E": 14, "F": 15}
        hex = self.getHex()
        assert(len(hex)) % 2 == 0, "Hex Value must be 2 digit or multiply of 2."
        hexArray = list(hex)
        for i in range(0,len(hexArray)):
            dec += 16**(len(hexArray)-i-1)*map[hexArray[i]]
            if verbose:
                print("DEBUG_convertHexDec: Eintrag ", i, "in hexArray: ", hexArray[i])
                print("DEBUG_convertHexDec: mappt auf : ", map[hexArray[i]])
        self.setDec(dec)
        self.changed("dec")
        if verbose:
            print("DEBUG_convertHexDec: Converted ", hex, "to ", dec)
    def convertHexBin(self):
        self.convertHexDec()
        self.convertDecBin()
        self.changed("bin")
    def update(self):
        if self.last_changed() == "dec":
            self.convertDecBin()
            self.convertBinHex()
        elif self.last_changed() == "hex":
            self.convertHexDec()
            self.convertHexBin()
        else:
            self.convertBinDec()
            self.convertBinHex()